var cubePrefab : Transform;
var cubeMaterial : Material;
var text : GUIText;

var startPosition : Transform;

private var score : int;
public var ready : boolean = false;

function Start()
{	
	if(cubeMaterial.color == Color.red)
	{
		score = PlayerPrefs.GetInt("balls_redCubes");
	}
	else if(cubeMaterial.color == Color.green)
	{
		score = PlayerPrefs.GetInt("balls_greenCubes");
	}
	else
	{
		Debug.LogWarning("Wrong Material selected!");
	}
	
	
	for(var i=0; i<=score; i++)
	{
		var tempCube = Instantiate(cubePrefab, startPosition.position, Quaternion.Euler(45, 0, 45));
		tempCube.transform.renderer.material = cubeMaterial;
		text.guiText.text = i.ToString();
		if(i < 225)
		{
			text.pixelOffset.y += 1.1;
		}
		yield WaitForSeconds(0.05);
	}
	
	ready = true;
}