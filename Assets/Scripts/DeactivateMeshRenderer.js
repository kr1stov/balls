#pragma strict

function Start () {
	var mrs = GetComponentsInChildren(MeshRenderer);
	
	for(var m : MeshRenderer in mrs)
	{
		m.enabled = false;
	}
}