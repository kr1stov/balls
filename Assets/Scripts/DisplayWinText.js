#pragma strict

public var green : GameObject;
public var red : GameObject;
public var restartText : GUIText;

function Start () {
	restartText.gameObject.active = false;
	guiText.text = " ";
}

function Update () {
	if(green.GetComponent(CollectedCubes).ready && red.GetComponent(CollectedCubes).ready)
	{
		if(PlayerPrefs.GetInt("balls_redCubes") > PlayerPrefs.GetInt("balls_greenCubes"))
		{
			guiText.material.color = Color.red;
			guiText.text = "RED WINS";
		}
		else if(PlayerPrefs.GetInt("balls_redCubes") < PlayerPrefs.GetInt("balls_greenCubes"))
		{
			guiText.material.color = Color.green;
			guiText.text = "GREEN WINS";
		}
		else
		{
			guiText.material.color = Color.black;
			guiText.text = "DRAW";
		}
		
		restartText.gameObject.active = true;
	}
}