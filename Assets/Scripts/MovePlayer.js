#pragma strict

public var speed : float;
private var moveDirection : Vector3;

public enum PlayerID { One, Two}

public var id : PlayerID = PlayerID.One;

function Start () {
	moveDirection = Vector3.zero;
}

function Update () {
	
	MovePlayer(id);
	
	moveDirection *= speed;
	rigidbody.AddForce(moveDirection);
}

function OnCollisionEnter(collision : Collision)
{
	if(collision.gameObject.tag == "cube")
	{
		collision.gameObject.renderer.material.color = renderer.material.color;
	}
}

function MovePlayer(p : PlayerID)
{
	if(p == PlayerID.Two)
	{
		if(Input.GetKey(KeyCode.LeftArrow))
		{
			moveDirection.x = -1;
		}
		else if(Input.GetKey(KeyCode.RightArrow))
		{
			moveDirection.x = 1;
		}
		else
		{
			moveDirection.x = 0;
		}
		
		if(Input.GetKey(KeyCode.UpArrow))
		{
			moveDirection.z = 1;
		}
		else if(Input.GetKey(KeyCode.DownArrow))
		{
			moveDirection.z = -1;
		}
		else
		{
			moveDirection.z = 0;
		}
	}
	else if(p == PlayerID.One)
	{
		if(Input.GetKey(KeyCode.A))
		{
			moveDirection.x = -1;
		}
		else if(Input.GetKey(KeyCode.D))
		{
			moveDirection.x = 1;
		}
		else
		{
			moveDirection.x = 0;
		}
		
		if(Input.GetKey(KeyCode.W))
		{
			moveDirection.z = 1;
		}
		else if(Input.GetKey(KeyCode.S))
		{
			moveDirection.z = -1;
		}
		else
		{
			moveDirection.z = 0;
		}
	}
}
