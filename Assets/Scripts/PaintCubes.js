#pragma strict

var numberOfCubes : int;

function OnCollisionEnter(collision : Collision)
{
	if(numberOfCubes > 0 && gameObject.tag == "cube" && renderer.material.color != Color.white)
	{
		if(collision.gameObject.tag == "cube")
		{
			collision.gameObject.renderer.material.color = renderer.material.color;
			numberOfCubes--;
		}
	}
}