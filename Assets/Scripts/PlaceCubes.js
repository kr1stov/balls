public var cubePrefab : Transform;
public var cubes : int;

private var i : int = 0;
private var j : int = 0;

function Start()
{
	PlayerPrefs.SetInt("balls_cubes", Mathf.Pow(cubes, 2));
	
	for(i=0; i<cubes; i++)
	{
		for(j=0; j < cubes; j++)
		{
			Instantiate(cubePrefab, Vector3(-cubes/2 + i, 0, -cubes/2 + j), Quaternion.identity);
		}
	}
}
