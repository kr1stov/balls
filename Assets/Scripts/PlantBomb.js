#pragma strict

var prefab : Transform;
var position : Vector3;

private var mp : MovePlayer;
private var temp : Transform;

function Start()
{
	mp = GetComponent(MovePlayer);
}

function Update () 
{
	if(mp.id == PlayerID.Two)
	{
		if(Input.GetKeyUp(KeyCode.RightControl))
		{
			temp = Instantiate(prefab, transform.position, Quaternion.identity);
			temp.renderer.material.color = renderer.material.color;
		}
	}
	else if(mp.id == PlayerID.One)
	{
		if(Input.GetKeyUp(KeyCode.E))
		{
			temp = Instantiate(prefab, transform.position, Quaternion.identity);
			temp.renderer.material.color = renderer.material.color;
		}
	}
}