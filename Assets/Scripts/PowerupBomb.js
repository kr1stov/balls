#pragma strict

var timeSpawned : float;
var radius = 5.0;
var power = 10.0;

function Start () 
{
	/*timeSpawned = Time.timeSinceLevelLoad;
	renderer.mater.color = Color.red;
	*/
	var explosionPos : Vector3 = transform.position;
    var colliders : Collider[] = Physics.OverlapSphere (explosionPos, radius);
    
    for (var hit : Collider in colliders) 
    {
        if (!hit)
            continue;
        
        if (hit.rigidbody)
        {
            hit.gameObject.renderer.material.color = renderer.material.color;
            hit.rigidbody.AddExplosionForce(power, explosionPos, radius, 3.0);
        }
    }
}

function Update () 
{
	/*if(Time.timeSinceLevelLoad - timeSpawned > 3)
	{*/
		
}