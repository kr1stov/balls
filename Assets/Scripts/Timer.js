#pragma strict

public var isActive : boolean = true;
public var secondsRemaining : int;
private var timeEnd : int;

private var redCubes : int;
private var greenCubes : int;

function Start () {
	timeEnd = Time.timeSinceLevelLoad + secondsRemaining;
}

function Update () {
	guiText.text = secondsRemaining.ToString();
	
	if(isActive)
	{
		secondsRemaining = timeEnd - Time.timeSinceLevelLoad;
		
		if(secondsRemaining == 0)
		{
			var cubes = GameObject.FindGameObjectsWithTag("cube");
			
			for(var c : GameObject in cubes)
			{
				if(c.transform.renderer.material.color == Color.green)
				{
					greenCubes++;
					
				}
				else if(c.transform.renderer.material.color == Color.red)
				{
					redCubes++;
				}
			}
			
			PlayerPrefs.SetInt("balls_redCubes", redCubes);
			PlayerPrefs.SetInt("balls_greenCubes", greenCubes);
			
			Application.LoadLevel("Result");
		}
	}
}